package helper

import (
	"reflect"

	"github.com/gorilla/websocket"
)

func GetWebSocketFD(conn *websocket.Conn) int {
	tcpConn := reflect.Indirect(reflect.ValueOf(conn)).FieldByName("conn").Elem()
	netConn := reflect.Indirect(tcpConn).FieldByName("conn")
	fd := netConn.FieldByName("fd")
	pfd := reflect.Indirect(fd).FieldByName("pfd")
	return int(pfd.FieldByName("Sysfd").Int())
}
