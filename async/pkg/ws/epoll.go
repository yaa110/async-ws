package ws

import (
	"sync"

	"golang.org/x/sys/unix"
)

type Epoll struct {
	fd          int
	connections map[int]*Context
	lock        *sync.RWMutex
}

func NewEpoll() (*Epoll, error) {
	fd, err := unix.EpollCreate1(0)
	if err != nil {
		return nil, err
	}
	return &Epoll{
		fd:          fd,
		connections: make(map[int]*Context),
		lock:        &sync.RWMutex{},
	}, nil
}

func (ep *Epoll) Add(ctx *Context) error {
	err := unix.EpollCtl(ep.fd, unix.EPOLL_CTL_ADD, ctx.FD, &unix.EpollEvent{
		Events: unix.EPOLLIN | unix.EPOLLRDHUP | unix.EPOLLHUP,
		Fd:     int32(ctx.FD),
	})
	if err != nil {
		return err
	}
	ep.lock.Lock()
	defer ep.lock.Unlock()
	ep.connections[ctx.FD] = ctx
	return nil
}

func (ep *Epoll) Delete(ctx *Context) error {
	err := unix.EpollCtl(ep.fd, unix.EPOLL_CTL_DEL, ctx.FD, nil)
	ep.lock.Lock()
	defer ep.lock.Unlock()
	delete(ep.connections, ctx.FD)
	return err
}

func (ep *Epoll) Wait() ([]*Context, []bool, error) {
	events := make([]unix.EpollEvent, 100)
	n, err := unix.EpollWait(ep.fd, events, 100)
	if err != nil {
		return nil, nil, err
	}
	ep.lock.RLock()
	defer ep.lock.RUnlock()
	var contexts []*Context
	var closeStatuses []bool
	for i := 0; i < n; i++ {
		ctx, found := ep.connections[int(events[i].Fd)]
		if found {
			contexts = append(contexts, ctx)
			closeStatuses = append(closeStatuses, events[i].Events&unix.EPOLLRDHUP == unix.EPOLLRDHUP || events[i].Events&unix.EPOLLHUP == unix.EPOLLHUP)
		}
	}
	return contexts, closeStatuses, nil
}
