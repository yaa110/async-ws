package ws

import "github.com/gorilla/websocket"

import "git.arvan.me/workshop/pkg/helper"

type Context struct {
	FD   int
	Conn *websocket.Conn
}

func NewContext(conn *websocket.Conn) *Context {
	return &Context{
		FD: helper.GetWebSocketFD(conn),
		Conn: conn,
	}
}
