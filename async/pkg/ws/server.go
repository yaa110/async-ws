package ws

import (
	// "github.com/davecgh/go-spew/spew"
	"log"
	"net/http"
	"runtime"

	"git.arvan.me/workshop/pkg/helper"
	"github.com/gorilla/websocket"
)

type Server struct {
	upgrader websocket.Upgrader
	epoll    *Epoll
}

func NewServer() (*Server, error) {
	ep, err := NewEpoll()
	if err != nil {
		return nil, err
	}
	return &Server{
		upgrader: websocket.Upgrader{},
		epoll:    ep,
	}, nil
}

func (s *Server) closeContext(ctx *Context) {
	err := s.epoll.Delete(ctx)
	if err != nil {
		log.Println("Delete error:", err)
	}
	ctx.Conn.Close()
}

func (s *Server) wait() {
	for {
		contexts, closeStatuses, err := s.epoll.Wait()
		if err != nil {
			log.Println("wait error:", err)
			continue
		}
		for i, ctx := range contexts {
			if closeStatuses[i] {
				s.closeContext(ctx)
				continue
			}
			mt, message, err := ctx.Conn.ReadMessage()
			if err != nil {
				log.Println("read error:", err)
				s.closeContext(ctx)
				continue
			}
			log.Printf("%s: %s", ctx.Conn.RemoteAddr(), message)
			err = ctx.Conn.WriteMessage(mt, message)
			if err != nil {
				log.Println("write:", err)
				break
			}
		}
	}
}

func (s *Server) handle(w http.ResponseWriter, r *http.Request) {
	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("unable to upgrade connection:", err)
		return
	}
	// spew.Dump(conn)
	log.Printf("client: %s, fd: %d, goroutines: %d", r.RemoteAddr, helper.GetWebSocketFD(conn), runtime.NumGoroutine())
	err = s.epoll.Add(NewContext(conn))
	if err != nil {
		log.Println("add error:", err)
		conn.Close()
	}
}

func (s *Server) Listen(addr string) error {
	go s.wait()
	http.HandleFunc("/", s.handle)
	log.Println("start listening on", addr)
	return http.ListenAndServe(addr, nil)
}
