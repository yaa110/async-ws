package main

import (
	"git.arvan.me/workshop/pkg/ws"
	"log"
	"syscall"
)

func main() {
	var rlimit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rlimit); err != nil {
		log.Fatalln(err)
	}
	rlimit.Cur = rlimit.Max
	err := syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rlimit)
	if err != nil {
		log.Fatalln(err)
	}
	s, err := ws.NewServer()
	if err != nil {
		log.Fatalln(err)
	}
	log.Fatalln(s.Listen(":12180"))
}
