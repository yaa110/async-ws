module git.arvan.me/workshop

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/gorilla/websocket v1.4.1
	golang.org/x/sys v0.0.0-20191210023423-ac6580df4449
)
