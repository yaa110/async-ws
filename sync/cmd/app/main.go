package main

import (
	"git.arvan.me/workshop/pkg/ws"
	"log"
)

func main() {
	s := ws.NewServer()
	log.Fatalln(s.Listen(":12180"))
}
