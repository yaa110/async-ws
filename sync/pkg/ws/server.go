package ws

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"runtime"
)

type Server struct {
	upgrader websocket.Upgrader
}

func NewServer() *Server {
	return &Server{
		upgrader: websocket.Upgrader{},
	}
}

func (s *Server) handle(w http.ResponseWriter, r *http.Request) {
	conn, err := s.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("unable to upgrade connection:", err)
		return
	}
	defer conn.Close()
	log.Printf("client: %s, goroutines: %d", r.RemoteAddr, runtime.NumGoroutine())
	for {
		mt, message, err := conn.ReadMessage()
		if err != nil {
			log.Println("read error:", err)
			break
		}
		log.Printf("%s: %s", r.RemoteAddr, message)
		err = conn.WriteMessage(mt, message)
		if err != nil {
			log.Println("write:", err)
			break
		}
	}
}

func (s *Server) Listen(addr string) error {
	http.HandleFunc("/", s.handle)
	log.Println("start listening on", addr)
	return http.ListenAndServe(addr, nil)
}
